package com.github.axet.audiolibrary.encoders;

import com.github.axet.androidlibrary.sound.AudioTrack;
import com.github.axet.audiolibrary.app.RawSamples;

public interface Encoder {

    void encode(AudioTrack.SamplesBuffer buf, int pos, int len);

    void close();

}
